#[derive(Debug)]
pub struct Document {
    objects: Vec<Object>,
}

#[derive(Debug)]
pub struct Object {
    name: String,
    properties: Vec<Property>,
}

#[derive(Clone, PartialEq, Debug)]
pub enum Value {
    String(String),
    Bool(bool),
    Path(String),
    Int(i32),
}

#[derive(Clone, Debug)]
pub struct Property {
    name: String,
    value: Value,
}

impl Document {
    pub fn new() -> Document {
        Document { objects: vec![] }
    }

    pub fn add_object(&mut self, obj: Object) {
        self.objects.push(obj);
    }

    pub fn get_object(&self, name: &str) -> Option<&Object> {
        for obj in &self.objects {
            if obj.get_name() == name {
                return Some(obj);
            }
        }

        return None;
    }

    pub fn list_objects(&self) -> &Vec<Object> {
        &self.objects
    }

}

impl Object {
    pub fn new(name: &str) -> Object {
        Object{name: name.to_string(), properties: vec![]}
    }

    pub fn set_property(&mut self, name: &str, value: Value) {
        let prop = Property::new(name, value);
        self.properties.push(prop);
    }

    pub fn add_property(&mut self, property: Property) {
        self.properties.push(property);
    }

    pub fn get_name(&self) -> &str {
        &self.name.as_str()
    }

    pub fn get_property(&self, name: &str) -> Option<Value> {
        for prop in &self.properties {
            if prop.name.eq(name) {
                return Some(prop.value.clone());
            }
        }

        return None;
    }

    pub fn list_properties(&self) -> Vec<Property> {
        self.properties.clone()
    }

}

impl Property {
    pub fn new(name: &str, value: Value) -> Property {
        Property{name: name.to_string(), value}
    }

    pub fn get_name(&self) -> &str {
        &self.name.as_str()
    }

    pub fn get_value(&self) -> Value {
        self.value.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_object() {
        let o = Object::new("foo");
        assert_eq!("foo", o.get_name());
    }

    #[test]
    fn create_value() {
        let s = Value::String("foo".to_string());
        let p = Value::Path("/".to_string());
        let b = Value::Bool(true);
        let i = Value::Int(200);

        if let Value::String(sv) = s {
            assert_eq!("foo".to_string(), sv);
        } else {
            assert!(false, "No String in Value");
        }

        if let Value::Path(pv) = p {
            assert_eq!("/".to_string(), pv);
        } else {
            assert!(false, "No Path in Value");
        }

        if let Value::Bool(bv) = b {
            assert_eq!(true, bv);
        } else {
            assert!(false, "No Bool in Value");
        }

        if let Value::Int(iv) = i {
            assert_eq!(200, iv);
        } else {
            assert!(false, "No Int in Value");
        }
    }

    #[test]
    fn create_property() {
        let prop = Property::new("foo", Value::Int(200));
        assert_eq!("foo", prop.get_name());
        assert_eq!(Value::Int(200), prop.get_value());
    }

    #[test]
    fn full_object() {
        let mut o = Object::new("object");
        o.set_property("foo", Value::Int(200));
        assert_eq!("object", o.get_name());
        let prop = o.get_property("foo");
        match prop {
            Some(v)=> assert_eq!(Value::Int(200), v),
            None => assert!(false, "No property 'foo' found"),
        }
    }

}
