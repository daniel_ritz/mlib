mod rmlib;

pub use rmlib::*;

#[derive(Debug)]
pub enum ParseError {
    EOF,
    Expected(String),
    InvalidType,
    InvalidValue,
}

pub fn parse(text: &str) -> Result<Document, ParseError> {

    let mut document = Document::new();
    let mut lexer = Lexer::new(text);
    while !lexer.finished() {
        let obj = parse_object(&mut lexer);
        match obj {
            Ok(o) => {
                document.add_object(o);
            },
            Err(e) => return Err(e),
        }
    }

    Ok(document)
}

fn parse_object(lexer: &mut Lexer) -> Result<Object, ParseError> {

    let mut temp_name = String::new();
    while !lexer.finished() {
        match lexer.peek() {
            Some('\n') => lexer.next(),
            Some(' ') => lexer.next(),
            Some('{') => {
                lexer.next();
                let props = parse_object_body(lexer);
                match props {
                    Err(e) => return Err(e),
                    Ok(pp) => {
                        let mut obj = Object::new(&temp_name);
                        for p in pp {
                            obj.add_property(p);
                        }
                        return Ok(obj);
                    }
                }
                
            },
            Some(c) => {
                temp_name.push(c);
                lexer.next();
            }
            _ => break,
        }
    }

    Err(ParseError::EOF)
}

fn parse_object_body(lexer: &mut Lexer) -> Result<Vec<Property>, ParseError> {

    let mut props : Vec<Property> = vec![];
    while !lexer.finished() { 
        match lexer.peek() {
            Some(' ') => lexer.next(),
            Some('\t') => lexer.next(),
            Some('\n') => lexer.next(),
            Some('}') => {
                lexer.next();
                return Ok(props)
            },
            Some(_) => {
                let prop = parse_property(lexer);
                match prop {
                    Ok(p) => props.push(p),
                    Err(e) => return Err(e)
                }
            },
            _ => break,
        }
    }

    Err(ParseError::EOF)
}

fn parse_property(lexer: &mut Lexer) -> Result<Property, ParseError> {

    let mut temp_name = String::new();
    while !lexer.finished() {
        match lexer.peek() {
            Some(' ') => lexer.next(),
            Some('\t') => lexer.next(),
            Some('=') => {
                lexer.next();
                // parse value
                let value = parse_value(lexer);
                return match value {
                    Err(e) => Err(e),
                    Ok(value) => Ok(Property::new(&temp_name, value)), 
                }
            },
            Some(c) => {
                temp_name.push(c);
                lexer.next();
            }
            _ => break,
        }
    }

    Err(ParseError::EOF)

}

fn parse_value(lexer: &mut Lexer) -> Result<Value, ParseError> {
 
    // parse type
    let mut temp_type = String::new();
    while !lexer.finished() {
        match lexer.peek() {
            Some(' ') => lexer.next(),
            Some('\t') => lexer.next(),
            Some('(') => break,
            Some(c) => {
                temp_type.push(c);
                lexer.next();
            }
            _ => break,
        }
    }

    if lexer.peek() == None {
        return Err(ParseError::EOF)
    }

    if !lexer.expect('(') {
        return Err(ParseError::Expected("(".to_string()));
    }

    // parse value
    return match temp_type.as_str() {
        "string" => parse_string(lexer),
        "path" => parse_path(lexer),
        "int" => parse_int(lexer),
        "bool" => parse_bool(lexer),
        _ => Err(ParseError::InvalidType),
    }
}

fn parse_string(lexer: &mut Lexer) -> Result<Value, ParseError> {
    let mut temp_value = String::new();
    while !lexer.finished() {
        match lexer.peek() {
            Some(')') => {
                lexer.next();
                lexer.expect(';');
                temp_value = temp_value.replace("[[", "(");
                temp_value = temp_value.replace("]]", ")");
                return Ok(Value::String(temp_value));
            },
            Some(c) => {
                temp_value.push(c);
                lexer.next();
            }
            _ => break,
        }
    }

    Err(ParseError::EOF)
}

fn parse_int(lexer: &mut Lexer) -> Result<Value, ParseError> {
    match parse_string(lexer) {
        Err(e) => Err(e),
        Ok(Value::String(s)) => {
            let number = s.parse::<i32>();
            match number {
                Err(_) => Err(ParseError::InvalidValue),
                Ok(n) => Ok(Value::Int(n)),
            }
        },
        Ok(_) => panic!("Unexpected Value type"),
    }
}

fn parse_bool(lexer: &mut Lexer) -> Result<Value, ParseError> {
    match parse_string(lexer) {
        Err(e) => Err(e),
        Ok(Value::String(s)) => {
            match s.as_str() {
                "true" => Ok(Value::Bool(true)),
                "false" => Ok(Value::Bool(false)),
                _ => Err(ParseError::InvalidValue),
            }
        },
        Ok(_) => panic!("Unexpected Value type"),
    }
}

fn parse_path(lexer: &mut Lexer) -> Result<Value, ParseError> {
    match parse_string(lexer) {
        Err(e) => Err(e),
        Ok(Value::String(s)) => {
            Ok(Value::Path(s))
        },
        Ok(_) => panic!("Unexpected Value type"),
    }
}

struct Lexer<'a> {
    input: &'a str,
    pos: usize,
}

impl<'a> Lexer<'a> {
    pub fn new(text: &'a str) -> Lexer<'a> {
        Lexer { input: text, pos: 0 }
    }

    pub fn peek(&self) -> Option<char> {
        self.input.chars().nth(self.pos)
    }

    pub fn next(&mut self) {
        self.pos += 1;
    }

    pub fn finished(&self) -> bool {
        self.pos >= self.input.len()
    }

    pub fn expect(&mut self, c: char) -> bool {
        return if self.peek() != Some(c) {
            false
        } else {
            self.next();
            true
        }
    }

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_object() {
        let text = "obj1{\n}\nobj2 {\n}obj3{}"; 
        
        let doc = parse(text);

        assert!(doc.is_ok());
        let doc = doc.unwrap();
        let objects = doc.list_objects();
        assert_eq!(3, objects.len());

        assert_eq!("obj1", objects[0].get_name());
        assert_eq!("obj2", objects[1].get_name());
        assert_eq!("obj3", objects[2].get_name());
    }

    #[test]
    fn parse_property() {
        let text = "obj{\nfoo = string(Bar);    box  = string(Fox); num = int(20); b=bool(true);path=path(C:\\Users\\);\n}";

        let doc = parse(text);
        assert!(doc.is_ok());

        let doc = doc.unwrap();
        let objs = doc.list_objects();
        assert_eq!(1, objs.len());

        let obj = &objs[0];
        assert_eq!("obj", obj.get_name());
        assert_eq!(5, obj.list_properties().len());

        let foo = obj.get_property("foo");
        assert!(foo.is_some());
        let foo = foo.unwrap();
        assert_eq!(Value::String("Bar".to_string()), foo);

        let boxv = obj.get_property("box");
        assert!(boxv.is_some());
        let boxv = boxv.unwrap();
        assert_eq!(Value::String("Fox".to_string()), boxv);

        let num = obj.get_property("num");
        assert!(num.is_some());
        let num = num.unwrap();
        assert_eq!(Value::Int(20), num);

        let b = obj.get_property("b");
        assert!(b.is_some());
        let b = b.unwrap();
        assert_eq!(Value::Bool(true), b);

        let path = obj.get_property("path");
        assert!(path.is_some());
        let path = path.unwrap();
        assert_eq!(Value::Path("C:\\Users\\".to_string()), path);

        let wrong = obj.get_property("wrong");
        assert!(wrong.is_none());
    }

    #[test]
    fn brackets_in_props() {
        let text = "obj{s=string(Hello [[World]]);\np=path(C:\\Test [[ x ]]);}";

        let doc = parse(text);
        assert!(doc.is_ok());
        let doc = doc.unwrap();

        let obj = doc.get_object("obj");
        assert!(obj.is_some());
        let obj = obj.unwrap();

        let s = obj.get_property("s").unwrap();
        assert_eq!(Value::String("Hello (World)".to_string()), s);

        let p = obj.get_property("p").unwrap();
        assert_eq!(Value::Path("C:\\Test ( x )".to_string()), p);

    }
}
