#[derive(Clone, Debug)]
pub struct Track {
    title: Box<String>,
    artist: Option<Box<String>>,
    feat: Option<Box<String>>,
}

impl Track {

    pub fn new(title: &str) -> Track {
        Track { title: Box::new(title.to_string()), artist: None, feat: None }
    }

}

impl Track {

    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn artist(&self) -> Option<String> {
        match &self.artist {
            None => None,
            Some(artist) => {
                let artist = artist.clone();
                Some(*artist)
            }
        }
    }

    pub fn feat(&self) -> Option<String> {
        match &self.feat {
            None => None,
            Some(feat) => {
                let feat = feat.clone();
                Some(*feat)
            }
        }
    }

}

impl Track {

    pub fn with_artist(self, artist: &str) -> Track {
        Track { title: self.title, artist: Some(Box::new(artist.to_string())), feat: self.feat }
    }

    pub fn with_feat(self, feat: &str) -> Track {
        Track { title: self.title, artist: self.artist, feat: Some(Box::new(feat.to_string())) }
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_track() {
        let track = Track::new("hello, world");

        assert_eq!("hello, world", track.title());
        assert_eq!(None, track.artist());
        assert_eq!(None, track.feat());
    }

    #[test]
    fn artist_track() {
        let track = Track::new("hello, world");
        let track = track.with_artist("john doe");

        assert_eq!("hello, world", track.title());
        assert_eq!(Some("john doe".to_string()), track.artist());
        assert_eq!(None, track.feat());
    }

    #[test]
    fn feat_track() {
        let track = Track::new("hello, world");
        let track = track.with_artist("john doe");
        let track = track.with_feat("jane doe");

        assert_eq!("hello, world", track.title());
        assert_eq!(Some("john doe".to_string()), track.artist());
        assert_eq!(Some("jane doe".to_string()), track.feat());
    }

}