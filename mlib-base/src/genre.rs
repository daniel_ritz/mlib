#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GenreReference {
    id: Box<String>,
}

impl GenreReference {
    pub fn get(id: &str) -> GenreReference {
        GenreReference { id: Box::new(id.to_string()) }
    }
    pub fn id(&self) -> &str {
        &self.id
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Genre {
    name: Box<String>
}

impl Genre {
    pub fn new(name: &str) -> Self {
        Self { name: Box::new(String::from(name)) }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn change_name(&mut self, name: &str) {
        self.name = Box::new(String::from(name))
    }

}