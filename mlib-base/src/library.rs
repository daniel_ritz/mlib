use crate::{Album, AlbumMetadata, AlbumReference, Artist, ArtistReference, Genre, GenreReference};

pub trait Library<'a> {
    fn get_album(&self, reference: &AlbumReference) -> Result<Album, &'static str>;

    fn list_albums(&self) -> Vec<(AlbumReference, AlbumMetadata)>;
    fn list_albums_for_artist(
        &self,
        artist: &ArtistReference,
    ) -> Vec<(AlbumReference, AlbumMetadata)> {
        self.list_albums()
            .into_iter()
            .filter(|(_, metadata)| metadata.artist() == *artist)
            .collect()
    }
    fn list_albums_for_genre(
        &self,
        genre: &GenreReference,
    ) -> Vec<(AlbumReference, AlbumMetadata)> {
        self.list_albums()
            .into_iter()
            .filter(|(_, metadata)| match metadata.genre() {
                None => false,
                Some(g) => g == *genre,
            })
            .collect()
    }

    fn get_artist(&self, reference: &ArtistReference) -> Result<Artist, &'static str>;
    fn list_artists(&self) -> Vec<(ArtistReference, Artist)>;

    fn get_genre(&self, reference: &GenreReference) -> Result<Genre, &'static str>;
    fn list_genres(&self) -> Vec<(GenreReference, Genre)>;
}

pub trait EditableLibrary {
    fn update_album(&self, reference: AlbumReference, album: Album) -> Result<(), &'static str>;
}
