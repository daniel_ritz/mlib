use std::slice::Iter;

use crate::{ArtistReference, GenreReference, FileReference, Record, RecordType};

#[derive(Clone, PartialEq, Eq)]
pub struct AlbumReference {
    id: Box<String>,
}

impl AlbumReference {
    pub fn get(id: &str) -> AlbumReference {
        AlbumReference { id: Box::new(id.to_string()) }
    }
    pub fn id(&self) -> &str {
        &self.id
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct AlbumMetadata {
    title: Box<String>,
    artist: ArtistReference,
    genre: Option<GenreReference>,
    release_year: Option<u32>,
    cover_art: Vec<FileReference>,
}

impl AlbumMetadata {
    pub fn new(title: &str, artist: ArtistReference) -> AlbumMetadata {
        AlbumMetadata { title: Box::new(String::from(title)), artist, genre: None, release_year: None, cover_art: vec![] }
    }
}

impl AlbumMetadata {
    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn artist(&self) -> ArtistReference {
        self.artist.clone()
    }

    pub fn genre(&self) -> Option<GenreReference> {
        self.genre.clone()
    }

    pub fn release_year(&self) -> Option<u32> {
        self.release_year
    }

    pub fn cover_art(&self) -> Iter<FileReference> {
        self.cover_art.iter()
    }

}

impl AlbumMetadata {
    pub fn change_title(&mut self, title: &str) {
        self.title = Box::new(title.to_string())
    }

    pub fn change_artist(&mut self, artist: ArtistReference) {
        self.artist = artist
    }

    pub fn change_genre(&mut self, genre: Option<GenreReference>) {
        self.genre = genre
    }

    pub fn change_release_year(&mut self, release_year: Option<u32>) {
        self.release_year = release_year
    }

    pub fn add_cover_art(&mut self, file: FileReference) {
        self.cover_art.push(file)
    }

    pub fn remove_cover_art(&mut self, index: usize) -> Result<(), &str> {
        if index >= self.cover_art.len() { return Err("index out of bounds") }
        self.cover_art.remove(index);
        Ok(())
    }
}

pub struct Album {
    metadata: AlbumMetadata,
    records: Vec<Record>,
}

impl Album {
    pub fn new(title: &str, artist: ArtistReference) -> Album {
        Album { metadata: AlbumMetadata::new(title, artist), records: vec![] }
    }

    pub fn from_metadata(metadata: AlbumMetadata) -> Album {
        Album { metadata, records: vec![] }
    }
}

impl Album {
    pub fn title(&self) -> &str {
        self.metadata.title()
    }

    pub fn artist(&self) -> ArtistReference {
        self.metadata.artist()
    }

    pub fn genre(&self) -> Option<GenreReference> {
        self.metadata.genre()
    }

    pub fn release_year(&self) -> Option<u32> {
        self.metadata.release_year()
    }

    pub fn cover_art(&self) -> Iter<FileReference> {
        self.metadata.cover_art()
    }

    pub fn records(&self) -> Iter<Record> {
        self.records.iter()
    }

    pub fn metadata(&self) -> AlbumMetadata {
        self.metadata.clone()
    }

}

impl Album {
    pub fn change_title(&mut self, title: &str) {
        self.metadata.change_title(title)
    }

    pub fn change_artist(&mut self, artist: ArtistReference) {
        self.metadata.change_artist(artist)
    }

    pub fn change_genre(&mut self, genre: Option<GenreReference>) {
        self.metadata.change_genre(genre)
    }

    pub fn change_release_year(&mut self, release_year: Option<u32>) {
        self.metadata.change_release_year(release_year)
    }

    pub fn add_cover_art(&mut self, file: FileReference) {
        self.metadata.add_cover_art(file)
    }

    pub fn remove_cover_art(&mut self, index: usize) -> Result<(), &str> {
        self.metadata.remove_cover_art(index)
    }

    pub fn add_record(&mut self, record: Record) {
        self.records.push(record)
    }

    pub fn remove_record(&mut self, index: usize) -> Result<(), &str> {
        if index >= self.records.len() { return Err("index out of bounds") }
        self.records.remove(index);
        Ok(())
    }

    pub fn new_record(&mut self, record_type: RecordType) -> &mut Record {
        let record = Record::new(self.title(), record_type);
        self.add_record(record);
        self.records.last_mut().unwrap()
    }

}

#[cfg(test)]
mod tests {
    use crate::{RecordType, Track};

    use super::*;

    #[test]
    fn simple_album() {
        let album = Album::new("hello, world", ArtistReference::get("john doe"));
        assert_eq!("hello, world", album.title());
        assert_eq!(ArtistReference::get("john doe"), album.artist());
    }

    #[test]
    fn change_title() {
        let mut album = Album::new("hello, world", ArtistReference::get("john doe"));
        assert_eq!("hello, world", album.title());
        album.change_title("foo");
        assert_eq!("foo", album.title());
    }

    #[test]
    fn change_artist() {
        let mut album = Album::new("", ArtistReference::get("john doe"));
        assert_eq!(ArtistReference::get("john doe"), album.artist());
        album.change_artist(ArtistReference::get("jane doe"));
        assert_eq!(ArtistReference::get("jane doe"), album.artist());
    }

    #[test]
    fn change_genre() {
        let mut album = Album::new("", ArtistReference::get(""));
        assert_eq!(None, album.genre());
        album.change_genre(Some(GenreReference::get("rock")));
        assert_eq!(Some(GenreReference::get("rock")), album.genre());
    }
    
    #[test]
    fn change_release_year() {
        let mut album = Album::new("", ArtistReference::get(""));
        assert_eq!(None, album.release_year());
        album.change_release_year(Some(2023));
        assert_eq!(Some(2023), album.release_year());
    }

    #[test]
    fn add_records() {
        let mut album = Album::new("", ArtistReference::get(""));
        let foo = Record::new("foo", RecordType::CD);
        let bar = Record::new("bar", RecordType::DVD);
        album.add_record(foo);
        album.add_record(bar);

        assert_eq!(2, album.records().len());

        for record in album.records() {
            if record.title() == "foo" {
                assert_eq!(RecordType::CD, record.record_type());
            } else if record.title() == "bar" {
                assert_eq!(RecordType::DVD, record.record_type());
            } else {
                assert!(false, "unexpected record");
            }
        }

        assert_eq!(Ok(()), album.remove_record(0));
        assert_eq!(1, album.records().len());

        for record in album.records() {
            assert_eq!("bar", record.title());
            assert_eq!(RecordType::DVD, record.record_type());
        }

    }

    #[test]
    fn new_record() {
        let mut album = Album::new("hello, world", ArtistReference::get(""));
        let record = album.new_record(RecordType::CD);
        assert_eq!("hello, world", record.title());
        assert_eq!(RecordType::CD, record.record_type());
        record.add_track(Track::new("foo"));
        assert_eq!(1, record.tracks().len());

        assert_eq!(1, album.records().len());
        for record in album.records() {
            assert_eq!("hello, world", record.title());
            assert_eq!(1, record.tracks().len());
        }
    }

    #[test]
    fn add_cover_art() {
        let mut album = Album::new("", ArtistReference::get(""));
        album.add_cover_art(FileReference::get("/"));
        album.add_cover_art(FileReference::get("/tmp/"));
        assert_eq!(2, album.cover_art().len());

        for file in album.cover_art() {
            assert!(if *file == FileReference::get("/") || *file == FileReference::get("/tmp/") { true } else {false}, "unexpected file reference");
        }
    }
}