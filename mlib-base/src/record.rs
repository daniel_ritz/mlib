use std::slice::Iter;

use crate::track::Track;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum RecordType {
    Digital,
    CD,
    Tape,
    Vinyl,
    DVD,
}

#[derive(Clone, Debug)]
pub struct Record {
    record_type: RecordType,
    title: Box<String>,
    tracks: Vec<Track>,
}

impl Record {
    pub fn new(title: &str, record_type: RecordType) -> Record {
        Record { record_type, title: Box::new(title.to_string()), tracks: vec![] }
    }
}

impl Record {
    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn tracks(&self) -> Iter<Track> {
        self.tracks.iter()
    }

    pub fn record_type(&self) -> RecordType {
        self.record_type
    }
}

impl Record {
    pub fn add_track(&mut self, track: Track) {
        self.tracks.push(track);
    }

    pub fn remove_track(&mut self, index: usize) -> Result<(), &str> {
        if index >= self.tracks.len() { return Err("out of bounds")}
        self.tracks.remove(index);
        Ok(())
    }

    pub fn change_record_type(&mut self, record_type: RecordType) {
        self.record_type = record_type;
    }

    pub fn change_title(&mut self, title: &str) {
        self.title = Box::new(title.to_string());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_record() {
        let record = Record::new("hello, world", RecordType::CD);
        assert_eq!("hello, world", record.title());
        assert_eq!(RecordType::CD, record.record_type());
        assert_eq!(0, record.tracks().len());
    }

    #[test]
    fn complex_record() {
        let mut record = Record::new("hello, world", RecordType::Tape);
        record.add_track(Track::new("foo"));
        record.add_track(Track::new("bar").with_artist("baz"));

        assert_eq!(2, record.tracks().len());
        let tracks = record.tracks();
        for track in tracks {
            if track.title() == "foo" {
                assert_eq!(None, track.artist());
            } else if track.title() == "bar" {
                assert_eq!(Some("baz".to_string()), track.artist());
            } else {
                assert!(false, "Unexpected track");
            }
        }
    }

    #[test]
    fn change_record_type() {
        let mut record = Record::new("", RecordType::CD);
        assert_eq!(RecordType::CD, record.record_type());
        record.change_record_type(RecordType::Digital);
        assert_eq!(RecordType::Digital, record.record_type());
    }

    #[test]
    fn change_title() {
        let mut record = Record::new("hello, world", RecordType::CD);
        assert_eq!("hello, world", record.title());
        record.change_title("foo");
        assert_eq!("foo", record.title());
    }

}