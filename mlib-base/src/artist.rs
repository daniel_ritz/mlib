#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ArtistReference {
    id: Box<String>,
}

impl ArtistReference {
    pub fn get(id: &str) -> ArtistReference {
        ArtistReference { id: Box::new(id.to_string()) }
    }
    pub fn id(&self) -> &str {
        &self.id
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Artist {
    name: Box<String>,
}

impl Artist {
    pub fn new(name: &str) -> Artist {
        Artist { name: Box::new(String::from(name)) }
    }
}

impl Artist {
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Artist {
    pub fn change_name(&mut self, name: &str) {
        self.name = Box::new(String::from(name))
    }
}