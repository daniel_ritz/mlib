#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FileReference {
    id: Box<String>,
}

impl FileReference {
    pub fn get(id: &str) -> FileReference {
        FileReference { id: Box::new(id.to_string()) }
    }
    pub fn id(&self) -> &str {
        &self.id
    }
}