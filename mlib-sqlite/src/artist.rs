use mlib_base::{Artist, ArtistReference};
use rusqlite::{Result, Connection, params};
use uuid::Uuid;

pub fn insert(conn: &Connection, artist: &Artist) -> Result<ArtistReference> {
    let id = Uuid::new_v4().to_string();

    conn.execute(
        "insert into artist (id, name)
            values (?1, ?2)", 
            params![id, artist.name()])?;

    Ok(ArtistReference::get(&id))
}

pub fn find(conn: &Connection, id: &ArtistReference) -> Result<Artist> {
    let id = id.id();

    let mut stmt = conn.prepare(
        "select name from artist where id = ?1"
    )?;
    stmt.query_row(params![id], |row| {
        let name: String = row.get(0)?;
        Ok(Artist::new(&name))
    })
}

pub fn list(conn: &Connection) -> Result<Vec<(ArtistReference, Artist)>> {

    let mut stmt = conn.prepare("select id, name from artist")?;
    let artists = stmt.query_map([], |row| {
        let id: String = row.get(0)?;
        let name: String = row.get(1)?;
        Ok((ArtistReference::get(&id), Artist::new(&name)))
    })?;

    let mut artist_vec = vec![];
    for artist in artists {
        match artist {
            Err(_) => {}
            Ok(a) => artist_vec.push(a)
        }
    }

    Ok(artist_vec)
}

pub fn remove(conn: &Connection, id: &ArtistReference) -> Result<()> {

    conn.execute(
        "delete from artist where id = ?1",
        [id.id()]
    )?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlib_base::Artist;
    use rusqlite::Connection;
    use crate::{setup::setup_database, artist::{find, remove}};

    use super::{insert, list};

    fn get_connection() -> Connection {
        let conn = Connection::open_in_memory().unwrap();
        setup_database(&conn).unwrap();
        return conn
    }


    #[test]
    fn test_artist() {
        let conn = get_connection();

        let john_doe = Artist::new("John Doe");
        let jane_doe = Artist::new("Jane Doe");

        assert_eq!(0, list(&conn).unwrap().len());

        let john_doe_ref = insert(&conn, &john_doe).unwrap();
        assert_eq!(1, list(&conn).unwrap().len());

        let jane_doe_ref = insert(&conn, &jane_doe).unwrap();
        assert_eq!(2, list(&conn).unwrap().len());

        assert_eq!(john_doe, find(&conn, &john_doe_ref).unwrap());
        assert_eq!(jane_doe, find(&conn, &jane_doe_ref).unwrap());

        remove(&conn, &john_doe_ref).unwrap();
        assert_eq!(1, list(&conn).unwrap().len());
        assert_eq!(jane_doe, find(&conn, &jane_doe_ref).unwrap());

        remove(&conn, &jane_doe_ref).unwrap();
        assert_eq!(0, list(&conn).unwrap().len());

    }

}