use rusqlite::{Connection, Result};

pub fn setup_database(conn: &Connection) -> Result<()> {
    setup_artist(conn)?;
    setup_genre(conn)?;
    setup_album(conn)?;
    setup_record(conn)?;
    setup_track(conn)?;
    Ok(())
}

fn setup_artist(conn: &Connection) -> Result<()> {
    conn.execute(
        "create table if not exists artist (
            id text not null primary key,
            name text not null
        );", 
        [])?;
    Ok(())
}

fn setup_genre(conn: &Connection) -> Result<()> {
    conn.execute(
        "create table if not exists genre (
            id text not null primary key,
            name text not null
        );", 
        [])?;
    Ok(())
}

fn setup_album(conn: &Connection) -> Result<()> {
    conn.execute(
        "create table if not exists album (
            id text not null primary key,
            title text not null,
            artist text not null references artist(id),
            genre text references genre(id),
            release_year integer
        );", 
        [])?;
    Ok(())
}

fn setup_record(conn: &Connection) -> Result<()> {
    conn.execute(
        "create table if not exists record (
            album text not null references album(id),
            serial integer not null,
            type integer not null,
            title text not null,
            primary key (album, serial)
        );", 
        [])?;
    Ok(())
}

fn setup_track(conn: &Connection) -> Result<()> {
    conn.execute(
        "create table if not exists track (
            album text not null references album(id),
            record integer not null,
            num integer not null,
            title text not null,
            artist text,
            feat text,
            primary key (album, record, num)
        );", 
        [])?;
    Ok(())
}