use mlib_base::*;
use rusqlite::Connection;

use crate::{
    albums,
    artist,
    genre
};

pub struct Library {
    conn: Connection
}

impl Library {
    pub fn new(conn: Connection) -> Self {
        Self { conn }
    }
}

impl<'a> mlib_base::Library<'a> for Library {
    fn get_album(&self, reference: &AlbumReference) -> Result<Album, &'static str> {
        let metadata =
            albums::find(&self.conn, reference).or_else(|_| Err("Album not found"))?;

        // TODO: construct records and tracks
        let album = Album::from_metadata(metadata);

        Ok(album)
    }

    fn list_albums(&self) -> Vec<(AlbumReference, AlbumMetadata)> {
        albums::list(&self.conn).unwrap_or(vec![])
    }

    fn get_artist(&self, reference: &ArtistReference) -> Result<Artist, &'static str> {
        artist::find(&self.conn, &reference).or_else(|_| Err("Artist not found"))
    }

    fn list_artists(&self) -> Vec<(ArtistReference, Artist)> {
        artist::list(&self.conn).unwrap_or(vec![])
    }

    fn get_genre(&self, reference: &GenreReference) -> Result<Genre, &'static str> {
        genre::find(&self.conn, &reference).or_else(|_| Err("Genre not found"))
    }

    fn list_genres(&self) -> Vec<(GenreReference, Genre)> {
        genre::list(&self.conn).unwrap_or(vec![])
    }
}

