mod setup;
mod artist;
mod genre;
mod albums;
mod library;

use rusqlite::Connection;
use setup::setup_database;


fn get_connection(path: &str) -> Connection {
    let conn = Connection::open(path);
    let conn = conn.expect("Error connecting to the database");

    setup_database(&conn).expect("Error initialising the database");

    return conn
}

pub fn new(path: &str) -> impl mlib_base::Library {
    let conn = get_connection(path);
    library::Library::new(conn)
}
