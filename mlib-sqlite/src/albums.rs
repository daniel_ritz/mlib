use mlib_base::{AlbumMetadata, AlbumReference, ArtistReference, GenreReference};
use rusqlite::{params, types::ValueRef, Connection, Result, Row};
use uuid::Uuid;

pub fn insert(conn: &Connection, album: &AlbumMetadata) -> Result<AlbumReference> {
    let id = Uuid::new_v4().to_string();

    let genre = match album.genre() {
        None => None,
        Some(genre) => Some(genre.id().to_owned()),
    };
    conn.execute(
        "insert into album (id, title, artist, genre, release_year)
                values (?1, ?2, ?3, ?4, ?5)",
        params![
            id,
            album.title(),
            album.artist().id(),
            genre,
            album.release_year()
        ],
    )?;

    Ok(AlbumReference::get(&id))
}

pub fn find(conn: &Connection, id: &AlbumReference) -> Result<AlbumMetadata> {
    let id = id.id();

    let mut stmt = conn.prepare(
        "select title, artist, genre, release_year 
                from album 
                where id = ?1",
    )?;

    let metadata = stmt.query_row(params![id], |row| {
        let metadata = metadata_from_row(row)?;
        Ok(metadata)
    })?;

    Ok(metadata)
}

pub fn list(conn: &Connection) -> Result<Vec<(AlbumReference, AlbumMetadata)>> {
    let mut stmt = conn.prepare(
        "select title, artist, genre, release_year, id
                from album",
    )?;

    let rows = stmt.query_map([], |row| {
        let id: String = row.get(4)?;
        let metadata = metadata_from_row(row)?;
        Ok((AlbumReference::get(&id), metadata))
    })?;

    let albums = rows
        .filter(|row| row.is_ok())
        .map(|row| row.unwrap())
        .collect::<Vec<_>>();

    Ok(albums)
}

pub fn remove(conn: &Connection, id: &AlbumReference) -> Result<()> {
    conn.execute(
        "delete from album where id = ?1",
        [id.id()]
    )?;

    Ok(())
}

fn metadata_from_row(row: &Row) -> Result<AlbumMetadata> {
    let title: String = row.get(0)?;
    let artist_id: String = row.get(1)?;
    let genre_id: Option<String> = match row.get_ref_unwrap(2) {
        ValueRef::Text(_) => Some(row.get(2)?),
        _ => None,
    };
    let release_year: Option<u32> = match row.get_ref_unwrap(3) {
        ValueRef::Integer(i) => Some(u32::try_from(i).unwrap_or(0)),
        _ => None,
    };
    let mut metadata = AlbumMetadata::new(&title, ArtistReference::get(&artist_id));
    metadata.change_genre(match genre_id {
        None => None,
        Some(id) => Some(GenreReference::get(&id)),
    });
    metadata.change_release_year(release_year);
    Ok(metadata)
}

#[cfg(test)]
mod tests {
    use crate::{artist, setup::setup_database, albums::remove};
    use mlib_base::{AlbumMetadata, AlbumReference, Artist, ArtistReference};
    use rusqlite::Connection;

    use super::{find, insert, list};

    fn get_connection() -> Connection {
        let conn = Connection::open_in_memory().unwrap();
        setup_database(&conn).unwrap();
        return conn;
    }

    fn create_artists(conn: &Connection) -> (ArtistReference, ArtistReference) {
        let john = Artist::new("John Doe");
        let jane = Artist::new("Jane Doe");

        let john_ref = artist::insert(conn, &john).unwrap();
        let jane_ref = artist::insert(conn, &jane).unwrap();

        return (john_ref, jane_ref);
    }

    #[test]
    fn test_insert_find() {
        let conn = get_connection();
        let (john, jane) = create_artists(&conn);

        let a = AlbumMetadata::new("hello, world", jane.clone());
        let b = AlbumMetadata::new("foobar", john.clone());

        let a_ref = insert(&conn, &a).unwrap();
        let b_ref = insert(&conn, &b).unwrap();

        let a_found = find(&conn, &a_ref).unwrap();
        let b_found = find(&conn, &b_ref).unwrap();
        assert_eq!(a, a_found);
        assert_eq!(b, b_found);
    }

    #[test]
    fn test_find_error() {
        let conn = get_connection();

        let not_found = find(&conn, &AlbumReference::get("unknown"));
        assert!(not_found.is_err(), "unknown album found");
    }

    #[test]
    fn test_list() {
        let conn = get_connection();
        let (john, jane) = create_artists(&conn);

        let a = AlbumMetadata::new("hello, world", jane.clone());
        let b = AlbumMetadata::new("foobar", john.clone());

        let a_ref = insert(&conn, &a).unwrap();
        let b_ref = insert(&conn, &b).unwrap();

        let albums = list(&conn).unwrap();
        assert_eq!(2, albums.len());
        for (album_ref, metadata) in albums {
            if a_ref == album_ref {
                assert_eq!(jane, metadata.artist());
                assert_eq!("hello, world", metadata.title());
            } else if b_ref == album_ref {
                assert_eq!(john, metadata.artist());
                assert_eq!("foobar", metadata.title());
            } else {
                panic!("unexpected album reference")
            }
        }
    }

    #[test]
    fn test_drop() {
        let conn = get_connection();
        let (john, _) = create_artists(&conn);

        let a = AlbumMetadata::new("hello, world", john.clone());
        let a_ref = insert(&conn, &a).unwrap();

        assert_eq!(1, list(&conn).unwrap().len());
        remove(&conn, &a_ref).unwrap();
        assert_eq!(0, list(&conn).unwrap().len());
    }
}
