use mlib_base::{GenreReference, Genre};
use rusqlite::{Connection, Result, params};
use uuid::Uuid;

pub fn insert(conn: &Connection, genre: &Genre) -> Result<GenreReference> {
    let id = Uuid::new_v4().to_string();

    conn.execute(
        "insert into genre (id, name)
            values (?1, ?2)", 
            params![id, genre.name()])?;

    Ok(GenreReference::get(&id))
}

pub fn find(conn: &Connection, id: &GenreReference) -> Result<Genre> {

    let id = id.id();

    let mut stmt = conn.prepare(
        "select name from genre where id = ?1"
    )?;
    stmt.query_row(params![id], |row| {
        let name: String = row.get(0)?;
        Ok(Genre::new(&name))
    })
    
}

pub fn list(conn: &Connection) -> Result<Vec<(GenreReference, Genre)>> {

    let mut stmt = conn.prepare("select id, name from genre")?;
    let genres = stmt.query_map([], |row| {
        let id: String = row.get(0)?;
        let name: String = row.get(1)?;
        Ok((GenreReference::get(&id), Genre::new(&name)))
    })?;

    let mut genre_vec = vec![];
    for genre in genres {
        match genre {
            Err(_) => {}
            Ok(a) => genre_vec.push(a)
        }
    }

    Ok(genre_vec)
}

pub fn remove(conn: &Connection, id: &GenreReference) -> Result<()> {

    conn.execute(
        "delete from genre where id = ?1",
        [id.id()]
    )?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlib_base::Genre;
    use rusqlite::Connection;
    use crate::{setup::setup_database, genre::{find, remove}};

    use super::{insert, list};

    fn get_connection() -> Connection {
        let conn = Connection::open_in_memory().unwrap();
        setup_database(&conn).unwrap();
        return conn
    }


    #[test]
    fn test_genre() {
        let conn = get_connection();

        let rock = Genre::new("rock");
        let roll = Genre::new("roll");

        assert_eq!(0, list(&conn).unwrap().len());

        let rock_ref = insert(&conn, &rock).unwrap();
        assert_eq!(1, list(&conn).unwrap().len());

        let roll_ref = insert(&conn, &roll).unwrap();
        assert_eq!(2, list(&conn).unwrap().len());

        assert_eq!(rock, find(&conn, &rock_ref).unwrap());
        assert_eq!(roll, find(&conn, &roll_ref).unwrap());

        remove(&conn, &rock_ref).unwrap();
        assert_eq!(1, list(&conn).unwrap().len());
        assert_eq!(roll, find(&conn, &roll_ref).unwrap());

        remove(&conn, &roll_ref).unwrap();
        assert_eq!(0, list(&conn).unwrap().len());

    }

}